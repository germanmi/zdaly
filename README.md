**Map Drawing Tool**

Consists of:
- LineString tool
*Hand draw line string*
- Polygon tool
*Hand draw polygon, double click to finish*
- Point marker tool
*Add point marker*
- Deletion tool
*Deletes any selected feature*
- Combine tool
*Groups features on the map*
- Uncombine tool
*Ungroups features on the map*
- Rotate mode:
*When turned on click and drag any object on the map to rotate it*
- Scale mode:
*When turned on click and drag any object on the map to scale it*
- Import features:
*Import .pdf files as images, .dxf, .svg, and .geojson as polygons. All elements except of polygons are ignored*
- Draw rectangle:
*Draw rectangle of predifined dimensions, aligned to your current bearing*
- Draw circle:
*Draw circle of predefined radius*
- Shape filling:
*Set current color, for the selected shape or next shape that will be created*
- Layer in stack:
*Set current layer, for the selected shape or next shape that will be created*
- Object opacity:
*Set current shape opacity, for the selected shape or next shape that will be created*